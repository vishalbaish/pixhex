import React,{useState} from 'react'
import Footer from '../Footer/Index'
import Navbar from '../Navbar'
import Row from '../Row/Row'
import ScrollToTop from '../ScrollToTop'
import { Data2d } from '../ServicesContent/Data'
import Sidebar from '../Sidebar'

function Anim2d() {

    const [isOpen, setIsOpen] = useState(false);
        
    const toggle = () => {
        setIsOpen(!isOpen)
    }

    const Movies2d = [
        {
            url : require('../../videos/(1).mp4').default,
            Poster: require("../../images/2d/1.png").default
        },
        {
            url : require('../../videos/(2).mp4').default,
            Poster: require("../../images/2d/2.png").default
        },
        {
            url : require('../../videos/(3).mp4').default,
            Poster: require("../../images/2d/3.png").default
        },
        {
            url : require('../../videos/(4).mp4').default,
            Poster: require("../../images/2d/4.png").default
        },
        {
            url : require('../../videos/(5).mp4').default,
            Poster: require("../../images/2d/5.png").default
        },
        {
            url : require('../../videos/(6).mp4').default,
            Poster: require("../../images/2d/6.png").default
        },
        {
            url : require('../../videos/(7).mp4').default,
            Poster: require("../../images/2d/7.png").default
        },
        {
            url : require('../../videos/(8).mp4').default,
            Poster: require("../../images/2d/8.png").default
        },
        {
            url : require('../../videos/(9).mp4').default,
            Poster: require("../../images/2d/9.png").default
        },
        {
            url : require('../../videos/(10).mp4').default,
            Poster: require("../../images/2d/10.png").default
        },
    
    ]
    

    return (
        <div id="services">
           <ScrollToTop />
            <Sidebar isOpen={isOpen} toggle= {toggle}/>
            <Navbar toggle={toggle} /> 
            <Row Movies={Movies2d} {...Data2d}/>
            <Footer />


        </div>
    )
}

export default Anim2d
