import React from 'react'
import Icon1 from '../../images/svg-2.svg'
import Icon2 from '../../images/svg-3.svg'
import Icon3 from '../../images/svg-4.svg'
import Zoom from 'react-reveal/Zoom';
import Fade from 'react-reveal/Fade';
import { ServicesContainer, ServicesCard, ServicesWrapper, ServicesIcon, ServicesH1, ServicesH2, ServicesP} from './ServicesElements'
const Services = () => {
    return (
        <ServicesContainer id='services'>
            <Zoom>
            <ServicesH1>Our Services</ServicesH1>
            </Zoom>
            <ServicesWrapper>
                    <Fade left>
                    <ServicesCard to='/vfx'>
                        <ServicesIcon src={Icon1} />
                        <ServicesH2>VFX</ServicesH2>
                        <ServicesP>supervisor will be available on set to assist you to craft the perfect VFX shot.</ServicesP>
                    </ServicesCard>
                    </Fade>
                    <Fade bottom>
                    <ServicesCard to='/Animation3d'>
                        <ServicesIcon src={Icon2} />
                        <ServicesH2>3D ANIMATION</ServicesH2>
                        <ServicesP>Help story tellers to bring their imaginary characters to life.</ServicesP>
                    </ServicesCard>
                    </Fade>
                    <Fade right>
                    <ServicesCard to='/Animation2d'>
                        <ServicesIcon src={Icon3} />
                        <ServicesH2>MOTION DESIGN</ServicesH2>
                        <ServicesP>Help you to visualise the look and feel of a completed frame</ServicesP>
                    </ServicesCard>
                    </Fade>
            </ServicesWrapper>
        </ServicesContainer>
    )
}

export default Services
