import React from 'react'
import './Form.css'
import Zoom from 'react-reveal/Zoom';

function Form() {

    const src = require('../../images/contact.jpg').default

    return (
        <div className="body-form" id="contact" style={{
            backgroundSize: "cover",
            backgroundImage: `url(${src}
            )`,
            backgroundPosition: 'center',
        }}>
            <div className="background" >
            <Zoom>
  <div className="containerZx">
  
    <div className="screen">
      <div className="screen-header">
        <div className="screen-header-left">
          <div className="screen-header-button close"></div>
          <div className="screen-header-button maximize"></div>
          <div className="screen-header-button minimize"></div>
        </div>
        <div className="screen-header-right">
          <div className="screen-header-ellipsis"></div>
          <div className="screen-header-ellipsis"></div>
          <div className="screen-header-ellipsis"></div>
        </div>
      </div>
      <div className="screen-body">
        <div className="screen-body-item left">
          <div className="app-title">
            <span>CONTACT</span>
            <span>US</span>
          </div>
          <div className="app-contact">CONTACT INFO : +91 88 00 40 24 30</div>
        </div>
        <div className="screen-body-item">
          <div className="app-form">
            <div className="app-form-group">
              <input className="app-form-control" required="required" placeholder="NAME" />
            </div>
            <div className="app-form-group">
              <input className="app-form-control" required="required" placeholder="EMAIL" />
            </div>
            <div className="app-form-group">
              <input className="app-form-control" required="required" placeholder="CONTACT NO" />
            </div>
            <div className="app-form-group message">
              <input className="app-form-control" required="required" placeholder="MESSAGE" />
            </div>
            <div className="app-form-group buttons">
              <button className="app-form-button">CANCEL</button>
              <button className="app-form-button">SEND</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </Zoom>
</div>


        </div>
    )
}

export default Form
