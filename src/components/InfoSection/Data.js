export const homeObjOne = {
    id: 'about',
    lightBg: false,
    lightText: true,
    lightTextDesc: true,
    topLine: 'About Us',
    headline: `Best picture for everyone. It’s what drives us every day.

    `,
    description: `We are a studio focusing on creating visually stimulating experiences. We use different tools to create innovative, original visual narratives and custom content solutions.`,
    buttonLabel: 'Know More',
    imgStart: false,
    img: require('../../images/svg-1.svg').default,
    alt: '0',
    dark: true,
    primary: true,
    darkText: false,
    showButton: true,
};

export const aboutObjOne = {
    id: 'about',
    lightBg: false,
    lightText: true,
    lightTextDesc: true,
    topLine: '',
    headline: `ABOUT US`,
    description: `Pixhex studio, was incorporated in 2020, with an objective to redefine the experience of consuming visual content, specifically in the Indian scenario. This primarily involved two things. Firstly, shifting focus of companies looking to develop content towards quality as against quantity or quick turnaround, and secondly, understanding that content is more than just visuals, it is meant to be a holistic experience designed specifically for their customers, after which they emerge educated, enlightened, emotional, or all of the above.`,
    buttonLabel: 'Know More',
    imgStart: false,
    img: require('../../images/svg-6.svg').default,
    alt: '0',
    dark: true,
    primary: true,
    darkText: false,
    showButton: false,
};

export const aboutObjTwo = {
    id: 'about',
    lightBg: true,
    lightText: false,
    lightTextDesc: false,
    topLine: '',
    headline: ``,
    description: `We stand for our unwavering focus on creativity, cutting-edge technology and commitment to innovation. Clubbing these with our wagon of world-class talent pool, we are all geared up to woo the audiences and clients alike.The Indian market is an unexplored landscape in terms of defined visual experiences, and that gives a huge canvas to start painting on.
    Going forward, we seek to continue on our strategy of designing experiences that are made to evoke a response from the viewer. Focussing on the heart of the content constitutes the baby steps required to deliver an outstanding result. 
    
    We are a studio focusing on creating visually stimulating experiences. We use different tools to create innovative, original visual narratives and custom content solutions.`,
    buttonLabel: 'Know More',
    imgStart: true,
    img: require('../../images/svg-5.svg').default,
    alt: '0',
    dark: true,
    primary: true,
    darkText: true,
    showButton: false,
};