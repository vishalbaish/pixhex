import React, { useState } from 'react'
import "./Row.css";

function Row({ Movies, bgColor, fontColor, para, title }) {
    const [trailerUrl, setTrailerUrl] = useState("");
    
    
    const handleClick = (Movie) => {
        // if (trailerUrl && !picUrl) {
        //     setTrailerUrl('');
        // } else if (!trailerUrl && !picUrl) {
        //         setTrailerUrl(Movie.url); 
        // } else if (picUrl && !trailerUrl) {
        //     setPicUrl("");
        // } else if (!picUrl && !trailerUrl) {
        //     setPicUrl(Movie.Poster);
        // }
        if (trailerUrl) {
            setTrailerUrl("")
        } else {
            setTrailerUrl(Movie.url);
        }
    };

    return (
        <div style={{ background: `${bgColor}` }} className="row" id="rowId">
            <div className="heading">
            <h1 style={{ color: `${fontColor}`}}>{title}</h1>
            <p style={{ color: `${fontColor}`}}>{para}</p>   
            </div>
            

            <div className="row_posters">
                {Movies.map(Movie => (
                    <img
                    onClick={() => handleClick(Movie)}
                    className='row_poster'
                    src={Movie.Poster} alt="" />
                ))}

            </div >
            {trailerUrl && <video src={trailerUrl} controls autoPlay style={{ width: '100%', height: '390px' }} />}
            
            
        </div>
    )
}

export default Row
