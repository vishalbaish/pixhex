import React,{useState} from 'react'
import Banner from '../Banner/Banner'
import Footer from '../Footer/Index'
import Navbar from '../Navbar'
import ScrollToTop from '../ScrollToTop'
import Sidebar from '../Sidebar'
import Card from './Card'


function ServicesPage() {

    const [isOpen, setIsOpen] = useState(false);
        
    const toggle = () => {
        setIsOpen(!isOpen)
    }
    

    return (
        <div id="services">
           <ScrollToTop />
            <Sidebar isOpen={isOpen} toggle= {toggle}/>
            <Navbar toggle={toggle} /> 
            <Banner type="lighting" ext="jpeg"/>
            <Card />
            <Footer />


        </div>
    )
}

export default ServicesPage
