import React from 'react'
import './Card.css'

function Card() {
        
    return (<>
            <div className='container'>
            <div className='card'>
                <div className='imgBx'>
                    <img src={require('../../images/lighting/1.jpeg').default} alt="" />
                </div>
                <div className='contentBx'>
                    <div className='content'>
                        <h3>Painting</h3>
                    </div>

                </div>
            </div>
            <div className='card'>
                <div className='imgBx'>
                    <img src={require('../../images/lighting/2.jpeg').default} alt="" />
                </div>
                <div className='contentBx'>
                    <div className='content'>
                        <h3>ROOM</h3>
                    </div>

                </div>
            </div>
            <div className='card'>
                <div className='imgBx'>
                    <img src={require('../../images/lighting/3.jpeg').default} alt="" />
                </div>
                <div className='contentBx'>
                    <div className='content'>
                        <h3>Post One</h3>
                    </div>

                </div>
            </div>
            <div className='card'>
                <div className='imgBx'>
                    <img src={require('../../images/lighting/4.jpeg').default} alt="" />
                </div>
                <div className='contentBx'>
                    <div className='content'>
                        <h3>Post One</h3>
                    </div>

                </div>
            </div>
            <div className='card'>
                <div className='imgBx'>
                    <img src={require('../../images/lighting/5.jpeg').default} alt="" />
                </div>
                <div className='contentBx'>
                    <div className='content'>
                        <h3>Post One</h3>
                    </div>

                </div>
            </div>
            <div className='card'>
                <div className='imgBx'>
                    <img src={require('../../images/lighting/6.jpeg').default} alt="" />
                </div>
                <div className='contentBx'>
                    <div className='content'>
                        <h3>Post One</h3>
                    </div>

                </div>
            </div>
            <div className='card'>
                <div className='imgBx'>
                    <img src={require('../../images/lighting/7.jpeg').default} alt="" />
                </div>
                <div className='contentBx'>
                    <div className='content'>
                        <h3>Post One</h3>
                    </div>

                </div>
            </div>
            <div className='card'>
                <div className='imgBx'>
                    <img src={require('../../images/lighting/8.jpeg').default} alt="" />
                </div>
                <div className='contentBx'>
                    <div className='content'>
                        <h3>Post One</h3>
                    </div>

                </div>
            </div>
            
        </div> 
        </>    
    )
}

export default Card

