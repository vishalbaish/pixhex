export const Data2d = {
        bgColor: '#000',
        fontColor: '#fff',
        title: '2D ANIMATION',
        para: 'PIX HEX animation studio offer a variety of 2d Animation services. We continually strive to keep our Animation up-to-date to ensure that your clients and Potential Customers like what they see. In a world where people’s attention spans have become shorter and the need for quick Information has increased, Focus animation studio understand your need for Animation that excites and inspires. We are proud our clients with high standard Animation that will not only please, but impress your viewers and keep them interested. FAS work quickly and diligently in order to create videos that you will love in the deadline that you need. 2D animation focuses on creating characters, storyboards, and backgrounds in a Two –Dimensional environment and creates movements in a Two-Dimensional artistic space. 2D Animation Videos have become an inevitable part of our present fast moving world. The benefits of 2D Animation are Innumerable.',
};

export const Data3d = {
    bgColor: '#000',
    fontColor: '#fff',
    title: '3D ANIMATION',
    para: 'PIX HEX animation studio offer 3D animation service. Whether you are looking for a landing page Video, simple or complex Motion Design, Explainer Videos, or Infographics, Our highly talented artist always meet the benchmark. 3d Animation is the process of developing or creating Three-Dimensional moving images in a Digital Environment. 3D Animation consists of 3D Media, Marketing Media, Web Media and Mobile Media. The benefits of 3D Animation is that it gives us the scope to incorporate various Effects and it can enhance the Entertainment level for the viewers. Another benefit of Three-Dimensional Animation is the part from enhanced Visual Effects, it can enhance your company’s brand image and Growth, Engaging customers, ability to grab more Attention, and given us a real feeling. 3D Animation gives the flexibility of telling a story of a product in an entirely new way. Our 3D team combines a multitude of discipline to output stunning Photorealistic 3D Renderings and Animations.',
};