import React,{useState} from 'react'
import Footer from '../Footer/Index'
import Navbar from '../Navbar'
import Row from '../Row/Row'
import ScrollToTop from '../ScrollToTop'
import { Data3d } from '../ServicesContent/Data'
import Sidebar from '../Sidebar'

function Anim3d() {

    const [isOpen, setIsOpen] = useState(false);
        
    const toggle = () => {
        setIsOpen(!isOpen)
    }

    const Movies3d = [
        {
            url : require('../../videos/3d/1.mp4').default,
            Poster: require("../../images/3d/1.png").default
        },
        {
            url : require('../../videos//3d/2.mp4').default,
            Poster: require("../../images/3d/2.png").default
        },
        {
            url : require('../../videos/3d/3.mp4').default,
            Poster: require("../../images/3d/3.png").default
        },
        {
            url : require('../../videos/3d/4.mp4').default,
            Poster: require("../../images/3d/4.png").default
        },
        {
            url : require('../../videos/3d/5.mp4').default,
            Poster: require("../../images/3d/5.png").default
        },
    
    ]
    

    return (
        <div id="services">
           <ScrollToTop />
            <Sidebar isOpen={isOpen} toggle= {toggle}/>
            <Navbar toggle={toggle} /> 
            <Row Movies={Movies3d} {...Data3d}/>
            <Footer />


        </div>
    )
}

export default Anim3d
