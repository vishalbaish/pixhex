import React, { useState } from 'react'
import Video from '../../videos/video.mp4'
import {Button} from '../ButtonElement'
import Fade from 'react-reveal/Fade';
import Zoom from 'react-reveal/Zoom';

import { HeroContainer, HeroBg, VideoBg,HeroContent, HeroH1, HeroP, HeroBtnWrapper, ArrowForward, ArrowRight } from './HeroElement'

const HeroSection = () => {
    const [hover, setHover] = useState(false)

    const onHover  = () => {
        setHover(!hover)
    }

    return (
        <HeroContainer>
            <HeroBg>
                <VideoBg autoPlay loop muted src={Video} height="100%" type='video/mp4' />
            </HeroBg>
            <HeroContent>
                <HeroH1>
                <Fade top>
                PIX HEX ANIMATION STUDIO    
                </Fade>
                </HeroH1>
                <Zoom>
                <HeroP>
                Creative design studio specialized in crafting unique digital experiences
                </HeroP>
                </Zoom>
                <Zoom>
                <HeroBtnWrapper>
                    <Button to='about'
                    smooth="true"
                    duration={500}
                    
                    exact='true'
                    offset={-80}
                    onMouseEnter={onHover} 
                    onMouseLeave={onHover}
                    dark='true' primary='true'
                    style={{ background: "-webkit-linear-gradient(to right, #f12711, #f5af19)"}}>
                        Get Started {hover ? <ArrowForward /> : <ArrowRight /> }
                    </Button>
                </HeroBtnWrapper>
                </Zoom>
            </HeroContent>
        </HeroContainer>
    )
}

export default HeroSection
