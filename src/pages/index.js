import React, { useState } from 'react'
import Footer from '../components/Footer/Index';
import Form from '../components/Form/Form';
import HeroSection from '../components/HeroSection';
import InfoSection from '../components/InfoSection';
import { homeObjOne } from '../components/InfoSection/Data';
import Navbar from '../components/Navbar'
import Services from '../components/Services';
import Sidebar from '../components/Sidebar'


const Home = () => {
    const [isOpen, setIsOpen] = useState(false);
    
    const toggle = () => {
        setIsOpen(!isOpen)
    }

    return (
        <>
        <Sidebar isOpen={isOpen} toggle= {toggle}/>
         <Navbar toggle={toggle} />
         <HeroSection />
         <InfoSection {...homeObjOne}/> 
         <Services /> 
         <Form />
         <Footer />
        </>
    )
}

export default Home
